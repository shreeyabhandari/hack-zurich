import React from "react";

import {
  FormGroup,
  Label,
  Input,
  FormText,
  Row,
  Col,
  Button
} from "reactstrap";
import MapRoute from "./DirectionRenderer";
class Forms extends React.Component  {
    
render(){
  return (
      <React.Fragment>
    <form>
    <Row>
    <Col>
      <FormGroup>
        <Label for="current-address">Current address</Label>
        <Input
          name="email"
          id="txtStartingPoint"
          placeholder="Enter Current Address"
        />
        <FormText color="muted">
          We'll never share your location with anyone.
        </FormText>
      </FormGroup>
      </Col>
      <Col>
      <FormGroup>
        <Label for="destination-location">Destination Location</Label>
        <Input
          type="text"
          name="destination-location"
          id="txtDestinationPoint"
          placeholder="Destination Location"
        />
      </FormGroup>
      </Col>
      </Row>    
      <Button id="btnQuery" value="Query"> Search</Button>
    </form>
    <MapRoute origin={"Paris"} destination={"bremen"}/>
    </React.Fragment>
  );
}
};

export default Forms;